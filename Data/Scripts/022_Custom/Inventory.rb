class Game_Temp
  attr_accessor :inventory_showing
  attr_accessor :inventory_selected
  attr_accessor :inventory_menu
  attr_accessor :sel_in_chest

  def inventory_showing=(value)
    @inventory_showing = value
    return @inventory_showing
  end

  def inventory_selected=(value)
    @inventory_selected = value
    return @inventory_selected
  end

  def inventory_selected
    @inventory_selected = 0 if !@inventory_selected
    return @inventory_selected
  end

  def inventory_menu=(value)
    @inventory_menu = value
    return @inventory_menu
  end

  def sel_in_chest
    @sel_in_chest = false if !@sel_in_chest
    return @sel_in_chest
  end
end

class Player < Trainer
  attr_accessor :inventory_size
  attr_accessor :stamina
  attr_accessor :max_stamina
  attr_accessor :day

  def inventory_size=(value)
    @inventory_size = value
    return @inventory_size
  end

  def inventory_size
    @inventory_size = 3 if !@inventory_size
    return @inventory_size
  end

  def modify_stamina(value)
    pbExhaust($game_player) if @stamina < 1
    @stamina += value
    @stamina = @stamina.clamp(0, @max_stamina)
    $game_temp.inventory_menu.pbUpdate
    return @stamina > 0
  end

  def gain_max_stamina
    @max_stamina += 10
    pbMessage("\\me[Key item get]You gained more NRG!\\wtnp[20]")
    pbMessage("Your max NRG is now at #{@max_stamina}!")
    @stamina = @max_stamina
    $game_temp.inventory_menu.pbUpdate
  end

  def stamina
    @stamina = 10 if !@stamina
    return @stamina
  end

  def max_stamina=(value)
    @max_stamina = value
    return @max_stamina
  end

  def max_stamina
    @max_stamina = 10 if !@max_stamina
    return @max_stamina
  end

  def day
    @day = 0 if !@day
    return @day
  end

  def next_day
    @day += 1
    return @day
  end
end

def pbToggleInventory(amt)
  pbPlayDecisionSE
  $game_temp.menu_calling = false
  $game_temp.inventory_selected += amt
  $game_temp.inventory_selected = 0 if $game_temp.inventory_selected >= $player.inventory_size
  $game_temp.inventory_selected = $player.inventory_size-1 if $game_temp.inventory_selected < 0
  $game_temp.inventory_menu.pbUpdate
end

def pbGainInventorySlot
  return if $player.inventory_size == 11
  $player.inventory_size+=1
  $game_temp.inventory_menu.pbUpdate
end

def pbCheckInventorySpace
  return $player.inventory_size > $bag.pockets[1].count
end

class Inventory
  def pbStartScene
    $game_temp.inventory_showing = true
    @viewport = Viewport.new(0, 0, Graphics.width, Graphics.height)
    @viewport.z = 99999
    @overlaysprite = BitmapSprite.new(Graphics.width, Graphics.height, @viewport)
    @overlaysprite.z = 999
    @sprites = {}
    pbUpdate
  end

  def pbUpdate
    pbDisposeSpriteHash(@sprites)
    @sprites = {}
    @overlaysprite.bitmap&.clear
    $player.inventory_size.times do |i|
      @sprites["inventory_slot_#{i}"] = IconSprite.new(0, 0, @viewport)
      @sprites["inventory_slot_#{i}"].setBitmap("Graphics/Pictures/Inventory/slot")
      @sprites["inventory_slot_#{i}"].x = 236+(44*(i-($player.inventory_size/2).round))
      @sprites["inventory_slot_#{i}"].x += 20 if $player.inventory_size%2 == 0
      @sprites["inventory_slot_#{i}"].y = 16
      @sprites["inventory_slot_#{i}"].opacity = 180
      @sprites["inventory_item_#{i}"] = ItemIconSprite.new(48, Graphics.height - 48, nil, @viewport)
      @sprites["inventory_item_#{i}"].x = @sprites["inventory_slot_#{i}"].x+18
      @sprites["inventory_item_#{i}"].y = 34
      @sprites["inventory_amount_#{i}"] = BitmapSprite.new(Graphics.width, Graphics.height, @viewport)
      @sprites["inventory_amount_#{i}"].z = 99
      @sprites["inventory_amount_#{i}"].bitmap.clear
      pbSetSmallFont(@sprites["inventory_amount_#{i}"].bitmap)
    end
    @sprites["stamina_bg"] = IconSprite.new(0, 0, @viewport)
    @sprites["stamina_bg"].setBitmap("Graphics/Pictures/Inventory/stamina_box")
    @sprites["stamina_bg"].x = 444
    @sprites["stamina_bg"].y = 60
    @sprites["stamina_bg"].opacity = 180
    @overlaysprite.opacity = 128
    h = $player.stamina * 84 / $player.max_stamina
    h = 1 if h < 1
    h = 84 - ((h / 2).round) * 2
    @overlaysprite.bitmap.blt(446, 96+h, AnimatedBitmap.new(_INTL("Graphics/Pictures/Inventory/stamina_fill")).bitmap, Rect.new(0, h, 60, 84))
    @sprites["sel"] = IconSprite.new(0, 0, @viewport)
    @sprites["sel"].setBitmap("Graphics/Pictures/Inventory/slot_sel")
    @sprites["sel"].y = 16
    @sprites["sel"].x = @sprites["inventory_slot_#{$game_temp.inventory_selected}"].x
    $player.inventory_size.times do |i|
      item = $bag.pockets[1][i]
      next unless item.is_a?(Array)
      @sprites["inventory_item_#{i}"].item = item[0]
      next if GameData::Item.get(item[0]).is_mega_stone?
      pbDrawOutlineText(@sprites["inventory_amount_#{i}"].bitmap, @sprites["inventory_slot_#{i}"].x-20, 40, 96, 32,_INTL("#{item[1]}x"), MessageConfig::LIGHT_TEXT_MAIN_COLOR, MessageConfig::LIGHT_TEXT_SHADOW_COLOR, 1)
    end
    @sprites["sel"].visible = !$game_temp.sel_in_chest
    @sprites["day"] = BitmapSprite.new(Graphics.width, Graphics.height, @viewport)
    @sprites["day"].z = 99
    @sprites["day"].bitmap.clear
    pbSetNarrowFont(@sprites["day"].bitmap)
    pbDrawOutlineText(@sprites["day"].bitmap, 8, 56, 96, 32, "Day: #{$player.day}", MessageConfig::LIGHT_TEXT_MAIN_COLOR, MessageConfig::LIGHT_TEXT_SHADOW_COLOR, 0)
    @sprites["money"] = BitmapSprite.new(Graphics.width, Graphics.height, @viewport)
    @sprites["money"].z = 99
    @sprites["money"].bitmap.clear
    pbSetNarrowFont(@sprites["money"].bitmap)
    pbDrawOutlineText(@sprites["money"].bitmap, 8, 88, 96, 32, "$#{$player.money}", MessageConfig::LIGHT_TEXT_MAIN_COLOR, MessageConfig::LIGHT_TEXT_SHADOW_COLOR, 0)
  end

  def pbEndScene
    $game_temp.inventory_showing = false
    pbDisposeSpriteHash(@sprites)
    @overlaysprite.bitmap.dispose
    @overlaysprite.dispose
    @viewport.dispose
  end

  def self.sell_stored_items
    if !$PokemonGlobal.pcItemStorage
      $PokemonGlobal.pcItemStorage = PCItemStorage.new
    end
    return false if $PokemonGlobal.pcItemStorage.empty?
    final_money = 0
    sold_items = 0
    $PokemonGlobal.pcItemStorage.items.each do |i|
      item = GameData::Item.get(i[0])
      item_cost = item.price
      item_amount = i[1]
      sold_items += item_amount
      final_money += item_cost * item_amount
    end
    item_or_items = sold_items == 1 ? "item" : "items"
    pbMessage("Jackie sold #{sold_items} #{item_or_items} overnight and got you $#{final_money}.")
    $player.money += final_money
    $game_temp.inventory_menu.pbUpdate
    pbMessage("\\me[SFX_PURCHASE]Added $#{final_money} to your wallet!")
    $PokemonGlobal.pcItemStorage = PCItemStorage.new
  end

  def self.get_sell_items
    if !$PokemonGlobal.pcItemStorage
      $PokemonGlobal.pcItemStorage = PCItemStorage.new
    end
    if $PokemonGlobal.pcItemStorage.empty?
      pbMessage("Jackie: I currently don't have any items that I am going to sell tonight.")
      return false
    end
    sell_items = [] #[[:ITEM1, 0], [:ITEM2, 15], [:ITEM3, 22]]
    $PokemonGlobal.pcItemStorage.items.each do |item|
      sell_items.push(item)
    end
    item_list = ""
    sell_items.each_with_index do |item, index|
      item_amount = item[1]
      next if item_amount < 1
      item_name = (item_amount == 1) ? GameData::Item.get(item[0]).name : GameData::Item.get(item[0]).name_plural
      item_list += "#{item_amount} #{item_name}"
      item_list += " and " if index == sell_items.length-2
      item_list += ", " unless index >= sell_items.length-2
    end
    pbMessage("Jackie: Overnight I will sell: #{item_list}.")
    return true
  end

  def self.get_sellable_items
    ret = []
    $bag.pockets[1].each do |i|
      next if !i.is_a?(Array) || i.nil?
      item = i[0]
      next if item.nil?
      item_obj = GameData::Item.get(item)
      next if item_obj.is_mega_stone?|| item_obj.is_snag_ball?
      ret.push(item_obj)
    end
    return ret
  end

  def self.sell_items
    sellables = Inventory.get_sellable_items
    choices = []
    sellables.each do |item|
      max_item_amount = $bag.quantity(item.id)
      choices.push("#{max_item_amount}x #{item.name}")
    end
    choices.push("Cancel")
    choice = pbMessage("Jackie: What would you like me to sell?",choices,choices.length-1)
    if choice == choices.length-1
      pbMessage("Jackie: If you would like me to sell anything all you have to do is ask.")
      return false
    end
    sel_item = sellables[choice]
    max_sell_amount = $bag.quantity(sel_item.id)
    amt = max_sell_amount
    if max_sell_amount > 1
      params = ChooseNumberParams.new
      params.setMaxDigits(((10**max_sell_amount) - 1))
      params.setRange(0, max_sell_amount)
      params.setDefaultValue(max_sell_amount)
      amt = pbMessageChooseNumber("Jackie: How many would you like me to sell?",params)
    end
    if amt == 0
      pbMessage("Jackie: If you would like me to sell anything all you have to do is ask.")
      return false
    end
    sel_item_name = (amt == 1) ? sel_item.name : sel_item.name_plural
    $bag.remove(sel_item.id, amt)
    $game_temp.inventory_menu.pbUpdate
    pbMessage("\\me[SFX_PURCHASE]Jackie: Tonight I will go to town and sell #{amt} #{sel_item_name}.")
    if !$PokemonGlobal.pcItemStorage
      $PokemonGlobal.pcItemStorage = PCItemStorage.new
    end
    $PokemonGlobal.pcItemStorage.add(sel_item.id, amt)
    return true
  end
end
