class Player < Trainer
  attr_accessor :chest

  def chest
    @chest = [] if !@chest
    return @chest
  end

  def chest=(value)
    @chest = value
    return @chest
  end

  def store_in_chest(item,amount=1)
    already_stored = false
    @chest.each_with_index do |c, index|
      if c.is_a?(Array) && c[0] == item
        already_stored = index
        break
      end
    end
    (already_stored == false) ? @chest.push([item, amount]) : @chest[already_stored][1] += amount
  end

  def remove_from_chest(item,amount=1)
    idx = -1
    @chest.each_with_index do |c, index|
      if c.is_a?(Array) && c[0] == item
        idx = index
        break
      end
    end
    return false if idx == -1
    return false if @chest[idx][1] < amount
    @chest[idx][1] -= amount
    @chest.delete_at(idx) if @chest[idx][1] < 1
    return true
  end

  def has_in_chest?(item, quantity=1)
    return quantity_in_chest?(item) >= quantity
  end

  def quantity_in_chest?(item)
    @chest.each_with_index do |c, index|
      next if !c.is_a?(Array) || c == []
      if c[0] == item
        return c[1]
      end
    end
    return 0
  end
end

class Chest
  def pbStartScene
    $game_temp.sel_in_chest = false
    @index_x = 0
    @index_y = -1
    @viewport = Viewport.new(0, 0, Graphics.width, Graphics.height)
    @viewport.z = 99999
    @disposed = false
    @sprites = {}
    3.times do |y|
      11.times do |x|
        @sprites["inventory_slot_#{x}_#{y}"] = IconSprite.new(0, 0, @viewport)
        @sprites["inventory_slot_#{x}_#{y}"].setBitmap("Graphics/Pictures/Inventory/slot")
        @sprites["inventory_slot_#{x}_#{y}"].x = 16 + 44 * x
        @sprites["inventory_slot_#{x}_#{y}"].y = 80 + 52 * y
        @sprites["inventory_slot_#{x}_#{y}"].opacity = 180
        @sprites["inventory_item_#{x}_#{y}"] = ItemIconSprite.new(48, Graphics.height - 48, nil, @viewport)
        @sprites["inventory_item_#{x}_#{y}"].x = @sprites["inventory_slot_#{x}_#{y}"].x + 18
        @sprites["inventory_item_#{x}_#{y}"].y = @sprites["inventory_slot_#{x}_#{y}"].y + 18
        @sprites["inventory_amount_#{x}_#{y}"] = BitmapSprite.new(Graphics.width, Graphics.height, @viewport)
        @sprites["inventory_amount_#{x}_#{y}"].z = 99
      end
    end
    @sprites["sel"] = IconSprite.new(0, 0, @viewport)
    @sprites["sel"].setBitmap("Graphics/Pictures/Inventory/slot_sel")
    pbUpdate
    pbInputs
  end

  def pbUpdate
    return if @disposed
    $game_temp.sel_in_chest = @index_y > -1
    3.times do |y|
      11.times do |x|
        @sprites["inventory_amount_#{x}_#{y}"].bitmap.clear
        pbSetSmallFont(@sprites["inventory_amount_#{x}_#{y}"].bitmap)
        item = $player.chest[x + 11 * y]
        if !item.is_a?(Array)
          @sprites["inventory_item_#{x}_#{y}"].item = nil
          next
        end
        @sprites["inventory_item_#{x}_#{y}"].item = item[0]
        next if GameData::Item.get(item[0]).is_mega_stone?
        pbDrawOutlineText(@sprites["inventory_amount_#{x}_#{y}"].bitmap, @sprites["inventory_slot_#{x}_#{y}"].x-20, @sprites["inventory_slot_#{x}_#{y}"].y+24, 96, 32,_INTL("#{item[1]}x"), MessageConfig::LIGHT_TEXT_MAIN_COLOR, MessageConfig::LIGHT_TEXT_SHADOW_COLOR, 1)
      end
    end
    if $game_temp.sel_in_chest
      @sprites["sel"].x = 16 + 44 * @index_x
      @sprites["sel"].y = 80 + 52 * @index_y
    end
    @sprites["sel"].visible = $game_temp.sel_in_chest
  end

  def pbInputs
    loop do
      Graphics.update
      Input.update
      $game_temp.inventory_menu.pbUpdate
      if Input.trigger?(Input::BACK)
        pbEndScene
        break
      elsif Input.trigger?(Input::USE)
        item = get_item_sel
        if item.nil? || !item.is_a?(Array)
          pbPlayBuzzerSE
        else
          pbPlayDecisionSE if move_item(item)
        end
        pbUpdate
      elsif Input.trigger?(Input::ACTION)
        item = get_item_sel
        if item.nil? || !item.is_a?(Array)
          pbPlayBuzzerSE
        else
          pbPlayDecisionSE if move_item(item,true)
        end
        pbUpdate
      elsif Input.trigger?(Input::UP) && @index_y > -1
        pbPlayCursorSE
        @index_y -= 1
        pbUpdate
      elsif Input.trigger?(Input::DOWN) && @index_y < 2
        pbPlayCursorSE
        @index_y += 1
        pbUpdate
      elsif Input.trigger?(Input::LEFT)
        pbPlayCursorSE
        if $game_temp.sel_in_chest
          @index_x -= 1
          @index_x = 10 if @index_x < 0
        else
          @index_x -= 1
          @index_x = $player.inventory_size-1 if @index_x < 0
          @index_x = @index_x.clamp(0, $player.inventory_size-1)
          $game_temp.inventory_selected = @index_x
          $game_temp.inventory_menu.pbUpdate
        end
        pbUpdate
      elsif Input.trigger?(Input::RIGHT)
        pbPlayCursorSE
        if $game_temp.sel_in_chest
          @index_x += 1
          @index_x = 0 if @index_x > 10
        else
          @index_x += 1
          @index_x = 0 if @index_x > $player.inventory_size-1
          @index_x = @index_x.clamp(0, $player.inventory_size-1)
          $game_temp.inventory_selected = @index_x
          $game_temp.inventory_menu.pbUpdate
        end
        pbUpdate
      end
    end
  end

  def get_item_sel
    ret = nil
    if $game_temp.sel_in_chest
      ret = $player.chest[@index_x + 11 * @index_y]
    else
      ret = $bag.pockets[1][$game_temp.inventory_selected]
    end
    return ret
  end

  def move_item(item, split=false)
    amt_to_move = (!split && Input.press?(Input::SHIFT)) ? item[1] : 1
    if split
      amt_to_move = (item[1]/2).ceil
      amt_to_move = 1 if amt_to_move < 1
    end
    if $game_temp.sel_in_chest
      if pbCheckInventorySpace || $bag.quantity(item[0]) > 0
        if $player.remove_from_chest(item[0],amt_to_move) && $bag.add(item[0],amt_to_move)
          return true
        end
      end
    else
      if $player.store_in_chest(item[0],amt_to_move) && $bag.remove(item[0],amt_to_move)
        return true
      end
    end
    return false
  end

  def pbEndScene
    @disposed = true
    pbDisposeSpriteHash(@sprites)
    @viewport.dispose
    $game_temp.sel_in_chest = false
  end
end