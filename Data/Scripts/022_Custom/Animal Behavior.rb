END_OF_LINE = [0, 20]

def pbCreateAnimals
  despawning_gows = 0
  $game_map.events.each_value do |event|
    if event.name[/Animal/i] && event.isOff?("A") && rand(2)==1
      event.moveto(rand(27..29), rand(19..21))
      event.direction = rand(1..4)*2
      $game_self_switches[[$game_map.map_id, event.id, "A"]] = true
      animal = Animal.new(event)
      event.setVariable(animal)
      $game_map.need_refresh = true
      event.character_name = "animal_#{animal.gender}"
      $game_map.need_refresh = true
      despawning_gows += 1
    end
    break if despawning_gows > 4
  end
  $game_map.need_refresh = true
end

class Animal
  attr_accessor :exists # true if the Animal exists
  attr_accessor :happiness # starts at 10 and maximum is 100
  attr_accessor :saturation # 0 means death of the Animal. 10 is maximum.
  attr_accessor :age # Days since birth of the Animal
  attr_accessor :event # Event of the Animal
  attr_accessor :nickname # Name of the Animal
  attr_accessor :gender # Gender of the Animal (0 = male, 1 = female)
  attr_accessor :despawn # Whether the Animal should despawn the next morning.
  attr_accessor :stones_ready # Whether the Animal has stones ready

  def initialize(event)
    @exists = true
    @despawn = true
    @happiness = 10
    @saturation = 5
    @age = 0
    @event = event
    @nickname = "Gow"
    @gender = rand(0..1)
    @stones_ready = false
    @event.character_name = "animal_#{@gender}"
  end

  def name;         return @nickname;     end
  def despawn;      return @despawn;      end
  def age;          return @age;          end
  def exists;       return @exists;       end
  def happiness;    return @happiness;    end
  def saturation;   return @saturation;   end
  def gender;       return @gender;       end
  def event;        return @event;        end
  def stones_ready; return @stones_ready; end

  def feed(by_player=true)
    return if !@exists
    max_happiness_gain = by_player ? 5 : 2
    @happiness += rand(0..max_happiness_gain)
    @saturation += 1
    if by_player
      @despawn = false
      $bag.remove(:CROP,1)
      $game_temp.inventory_menu.pbUpdate
      pbSEPlay("SFX_SNARE_9")
      pbMessage("You fed #{@nickname} some #{GameData::Item.get(:CROP).name}...\\wtnp[10]")
    end
    check_saturation
  end

  def fully_feed
    @saturation = 10
    check_saturation
  end

  def pet
    return if !@exists
    @happiness += rand(1..3)
    pbMessage("You petted #{@nickname}'s head gently...\\wtnp[10]")
    $scene.spriteset($game_map.map_id).addUserAnimation(8, @event.x, @event.y, true, 1)
  end

  def change_nickname
    @nickname = pbEnterNPCName("#{@nickname}'s new name...", 2, 16, @nickname, @event.character_name)
  end

  def collect_stones(message=true)
    return false if !@stones_ready
    amt = rand(1..3)
    amt += 1 if tame
    amt += 1 if @age > 50
    pbReceiveItem(:DROP,amt) if message
    @stones_ready = false
    return amt unless message
    return true
  end

  def age_up
    died = false
    @age += 1
    @saturation -= 1
    died = check_saturation
    died = :age if rand(100)+10 < @age
    death if died
    @stones_ready = true if !died && can_make_stones
    return died
  end

  def can_make_stones
    return @age > 3 && tame && @exists && @saturation > 2 && !@despawn
  end

  def tame
    return @happiness > 30
  end

  def hungry
    return @saturation < 10
  end

  def slay
    return false if !pbCheckInventorySpace && $bag.quantity(:LEATHER) < 1
    dropAmt = rand(1..3)
    dropAmt += 1 unless @despawn
    dropAmt += 1 if tame
    dropAmt += 1 if @age > 50
    death
    pbMessage("\\me[SFX_CAUGHT_MON]You slayed #{@nickname} for #{dropAmt} Leather...\\wtnp[10]")
    $bag.add(:LEATHER,dropAmt)
    $game_temp.inventory_menu.pbUpdate
    return true
  end

  def check_saturation
    if @saturation < 0
      death
      return :hunger
    elsif @saturation == 0
      @happiness -= 5
      return check_happiness
    elsif @saturation > 10
      @saturation = 10
    end
  end

  def check_happiness
    if @happiness < 0
      death
      return :unhappy
    elsif @happiness > 100
      @happiness == 100
    end
  end

  def death
    @exists = false
    $game_self_switches[[$game_map.map_id, @event.id, "A"]] = false
    @event.setVariable(nil)
    @event.character_name = ""
  end

  def message
    message = ""
    possible_messages = []
    he_she = @gender == 0 ? "he" : "she"
    his_her = @gender == 0 ? "his" : "her"
    him_her = @gender == 0 ? "him" : "her"
    if @despawn
      possible_messages = [
        "It's a wild #{@nickname}.",
        "The wild #{@nickname} is a bit scared.",
        "The wild #{@nickname} is on edge."
      ]
    else
      case
      when @happiness < 10 # Unhappy
        possible_messages = [
          "#{@nickname} seems very unhappy about #{his_her} environment.",
          "#{@nickname} is sad.",
          "#{@nickname} seems depressed.",
          "#{@nickname} is really scared.",
          "#{@nickname} can't get a break.",
          "#{@nickname} is really unhappy."
        ]
      when @happiness < 30 # Not tame
        possible_messages = [
          "#{@nickname} is going wild.",
          "#{@nickname} has energy left to spare.",
          "#{@nickname} isn't very fond of #{$player.name}.",
          "#{@nickname} seems afraid of #{$player.name}.",
          "#{$player.name} is scaring #{@nickname}.",
          "#{@nickname} is scared."
        ]
      when @happiness < 50 # Alright
        possible_messages = [
          "#{@nickname} is getting attached.",
          "#{@nickname} is looking at the grass.",
          "#{@nickname} is still a bit curious about #{$player.name}.",
          "#{@nickname} is getting along with #{$player.name}.",
          "#{@nickname} has #{his_her} head high.",
          "#{@nickname} is doing alright."
        ]
      when @happiness < 75 # Pretty chill
        possible_messages = [
          "#{@nickname} feels great.",
          "#{@nickname} is enjoying #{him_her}self.",
          "#{@nickname} is fond of #{$player.name}.",
          "#{@nickname} likes the company of #{$player.name}.",
          "#{@nickname} is happy.",
          "#{@nickname} is doing good."
        ]
      else # 75-100 happiness
        possible_messages = [
          "#{@nickname} feels amazing.",
          "#{@nickname} seems relaxed.",
          "#{@nickname} is very fond of #{$player.name}.",
          "#{@nickname} really likes the company of #{$player.name}.",
          "#{@nickname} is extremely happy.",
          "#{@nickname} is really happy to see #{$player.name}."
        ]
      end

      case
      when @saturation < 3
        possible_messages.push(
          "#{@nickname} is really hungry.",
          "#{@nickname} needs some food.",
          "#{@nickname} seems unnerved by the hunger.",
          "#{@nickname} seems to be salivating.",
          "#{@nickname} seems really skinny.",
          "#{@nickname} is looking really pale."
        )
      when @saturation < 5
        possible_messages.push(
          "#{@nickname} is a bit hungry.",
          "#{@nickname} could go for some food.",
          "#{@nickname} seems a bit unnerved by the hunger.",
          "#{@nickname} #{his_her} eyes are a bit watery.",
          "#{@nickname} seems a bit skinny.",
          "#{@nickname} is looking a little bit pale."
        )
      when @saturation < 8
        possible_messages.push(
          "#{@nickname} could go for a little snack.",
          "#{@nickname} is content with #{his_her} food."
        )
      else # 8 - 10 happiness
        possible_messages.push(
          "#{@nickname} is a bit full.",
          "#{@nickname} is stuffed."
        )
      end
    end

    message = possible_messages.sample
    return message
  end

  def find_path(map)
    path_tiles = []
    path_tiles.push(4) if map.get_tile(@event.x-1, @event.y, 0) == 387
    path_tiles.push(2) if map.get_tile(@event.x, @event.y-1, 0) == 387
    path_tiles.push(8) if map.get_tile(@event.x, @event.y+1, 0) == 387
    path_tiles.push(6) if map.get_tile(@event.x+1, @event.y, 0) == 387
    return path_tiles
  end

  def find_crop(map)
    crop_tiles = []
    tile = []
    [-2,-1, 0, 1, 2].each do |x|
      [-2,-1, 0, 1, 2].each do |y|
        tile = [@event.x + x, @event.y + y]
        next if tile[0] < 0 || tile[0] >= $game_map.width || tile[1] < 0 || tile[1] >= $game_map.height
        next if map.get_tile(tile[0], tile[1], 1) != 390
        next if x == 0 && y == 0
        crop_tiles.push(tile)
      end
    end
    return crop_tiles
  end

  def move
    return if !@exists
    char_name = (@age > 3) ? "gow_grown" : "animal"
    @event.character_name = "#{char_name}_#{@gender}"
    if @event.x == END_OF_LINE[0] && @event.y == END_OF_LINE[1] && !tame
      # Reached end of the line
      death
    end
    map = $map_factory.getMap($game_map.map_id)
    crop_tiles = find_crop(map)
    if map.get_tile(@event.x, @event.y, 1) == 390
      $scene.spriteset($game_map.map_id).addUserAnimation(Settings::GRASS_ANIMATION_ID, @event.x, @event.y, true, 1)
      map.erase_tile(@event.x, @event.y, 1)
      feed(false)
    end
    if crop_tiles.is_a?(Array) && crop_tiles != [] # Always go for crops if near
      selected_crop = crop_tiles.sample
      move_to_location(@event, selected_crop[0], selected_crop[1], false)
    end
    if rand(10) == 1 # 10% chance to ignore normal behavior
      @event.move_random
    else
      if distance_from_player > 1
        if tame
          rand(2)==1 ? @event.move_random : @event.move_toward_player
        elsif rand(8) == 1 # 16% chance to wander
          @event.move_random
        else
          if rand(2)==1 # 50% chance to find the path instead
            case find_path(map).sample
            when 2 then @event.move_down
            when 4 then @event.move_left
            when 6 then @event.move_right
            when 8 then @event.move_up
            end
          else
            @event.move_toward_position(END_OF_LINE[0], END_OF_LINE[1])
          end
        end
      else # player is closer than a few tiles
        player_holding = nil
        player_sel = $bag.pockets[1][$game_temp.inventory_selected]
        player_holding = player_sel[0] if player_sel.is_a?(Array) && player_sel != []
        if player_holding == :CROP || @happiness > 50
          @event.move_toward_player
        elsif rand(3) == 1 && !tame # 33% chance to prioritize destination
          @event.move_toward_position(END_OF_LINE[0], END_OF_LINE[1])
        else
          @event.move_away_from_player
        end
      end
    end
  end

  def distance_from_player
    relative = $map_factory.getThisAndOtherEventRelativePos(event, $game_player)
    return 10 unless relative
    return 0 if relative[0] == 0
    distance = relative[1] / relative[0]
    return distance.abs.round
  end

  def self.call_behavior(event)
    return if event.isOff?("A")
    animal = event.variable
    if animal.nil?
      $game_self_switches[[$game_map.map_id, event.id, "A"]] = false
      $game_map.need_refresh = true
      return
    end
    animal.move
  end

  def self.animal_count(tamed = false, hungry = false)
    ret = 0
    $game_map.events.each_value do |event|
      animal = event.variable
      next if animal.nil? || !animal.is_a?(Animal)
      next unless animal.exists
      next if animal.despawn && tamed
      ret += 1
    end
    return ret
  end

  def self.gaston_feed
    $game_map.events.each_value do |event|
      animal = event.variable
      next if animal.nil? || !animal.is_a?(Animal)
      next unless animal.exists && !animal.despawn
      animal.fully_feed
    end
  end


  def self.interact(event)
    return if event.isOff?("A")
    animal = event.variable
    sel_item = $bag.pockets[1][$game_temp.inventory_selected]
    if animal.nil?
      $game_self_switches[[$game_map.map_id, event.id, "A"]] = false
      $game_map.need_refresh = true
      return
    end
    if sel_item && sel_item.is_a?(Array) && sel_item[0] == :GOWREADER
      (animal.gender==0) ? gender_name = "male" : gender_name = "female"
      his_her = animal.gender == 0 ? "His" : "Her"
      if animal.despawn
        pbMessage("A wild #{gender_name} #{animal.name}. Saturation: #{animal.saturation}, Happiness: #{animal.happiness}, #{animal.age} Days of age.")
      else
        pbMessage("#{animal.name} is a tame #{animal.age} day old #{gender_name} Gow. #{his_her} happiness stands at #{animal.happiness}% and saturation is at #{animal.saturation*10}%")
      end
      return
    end
    if animal.despawn
      pbMessage(animal.message)
      if sel_item && sel_item.is_a?(Array) && sel_item[0] == :CROP
        animal.feed if pbConfirmMessage("Would you like to feed the wild #{animal.name}?")
      end
    else
      commands = ["Talk"]
      commands.push("Feed") if $bag.has?(:CROP)
      commands.push("Pet")
      commands.push("Nickname")
      commands.push("Collect Stones") if animal.stones_ready
      commands.push("Slay") if sel_item && sel_item.is_a?(Array) && [:MAINTOOL,:MAINTOOL2,:MAINTOOL3].include?(sel_item[0]) && (pbCheckInventorySpace || $bag.quantity(:LEATHER) > 0)
      commands.push("Release") if animal.age > 5
      commands.push("Cancel")
      command = pbMessage("What would you like to do with #{animal.name}?", commands, commands.length)
      case commands[command]
      when "Talk" then pbMessage(animal.message)
      when "Feed" then animal.feed
      when "Pet" then animal.pet
      when "Nickname" then animal.change_nickname
      when "Collect Stones" then animal.collect_stones
      when "Slay" then animal.slay
      when "Release" then animal.death
      when "Cancel" then return
      end
    end
  end
end