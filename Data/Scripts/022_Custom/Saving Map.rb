MAP_DATA_VARIABLE = 99

def pbSaveMapTileData(tile)
  $game_switches[MAP_DATA_VARIABLE] = [] if !$game_switches[MAP_DATA_VARIABLE].is_a?(Array)
  existing_tile = MapTile.get(tile.x, tile.y, tile.layer)
  if existing_tile
    $game_switches[MAP_DATA_VARIABLE][existing_tile] = tile
  else
    $game_switches[MAP_DATA_VARIABLE].push(tile)
  end
end

def pbLoadMapTileData
  return false unless $game_switches[MAP_DATA_VARIABLE].is_a?(Array) && $game_switches[MAP_DATA_VARIABLE] != []
  for tile in $game_switches[MAP_DATA_VARIABLE]
    thistile = $map_factory.getRealTilePos($game_player.map.map_id, tile.x, tile.y)
    map = $map_factory.getMap(thistile[0])
    tile_id = map.data[thistile[1], thistile[2], tile.layer]
    map.set_tile(thistile[1], thistile[2], tile.layer, tile.tile_id)
  end
end

def pbGetMapTileData(tile_id)
  ret = []
  return ret unless $game_switches[MAP_DATA_VARIABLE].is_a?(Array) && $game_switches[MAP_DATA_VARIABLE] != []
  for tile in $game_switches[MAP_DATA_VARIABLE]
    ret.push(tile) if tile.tile_id == tile_id
  end
  return ret
end

EventHandlers.add(:on_enter_map, :load_map_tile_data,
  proc { |_sender, e|
    pbLoadMapTileData
  }
)

class MapTile
  attr_accessor :x
  attr_accessor :y
  attr_accessor :layer
  attr_accessor :tile_id

  def initialize(x, y, layer, tile_id)
    @x = x
    @y = y
    @layer = layer
    @tile_id = tile_id
  end

  def x;       return @x;       end
  def y;       return @y;       end
  def layer;   return @layer;   end
  def tile_id; return @tile_id; end

  def self.get(x, y, layer)
    $game_switches[MAP_DATA_VARIABLE] = [] if !$game_switches[MAP_DATA_VARIABLE].is_a?(Array)
    return false unless $game_switches[MAP_DATA_VARIABLE].is_a?(Array) && $game_switches[MAP_DATA_VARIABLE] != []
    $game_switches[MAP_DATA_VARIABLE].each_with_index do |tile, index|
      return index if tile.x == x && tile.y == y && tile.layer == layer
    end
    return false
  end

  def self.compare(t, x, y, layer, tile_id)
    tile = t.is_a?(Array) ? t[0] : t
    return tile.x == x && tile.y == y && tile.layer == layer && tile.tile_id == tile_id
  end

  def set(tile_id)
    @tile_id = tile_id
  end
end