SONGLIST = [
  "RBY 133 The Road to Cerulean (Route 3)",
  "RBY 130 Mt. Moon",
  "RBY 138 The Road To Lavender (Route 11)",
  "RBY 109 Road to Viridian City (Route 1)",
  "RBY 123 Viridian Forest",
  "RBY 135 To Bill_s Origin From Cerulean (Route 24)",
  "RBY 145 Cycling",
  "RBY 146 Surf",
  "RBY 147 Cinnabar Island"
]

EventHandlers.add(:on_frame_update, :auto_music,
  proc { |event|
    next if $game_map.map_id != 2
    if Audio.bgm_pos != 0
      pbBGMFade(5) if rand(5000) == 1
    else
      if rand(100) == 1
        pbBGMPlay(SONGLIST[rand(0...SONGLIST.length)])
      end
    end
  }
)

LETTERSOUND = "SFX_SNARE_4"