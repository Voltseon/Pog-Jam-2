def pbCollector(x, y)
  gows_in_reach = find_gows(x, y)
  total_stones_gained = 0
  gows_in_reach.each do |event|
    animal = event.variable
    next if animal.nil? || !animal.is_a?(Animal)
    next unless animal.exists && animal.stones_ready
    total_stones_gained += animal.collect_stones(false)
  end
  if total_stones_gained > 0
    pbReceiveItem(:DROP, total_stones_gained)
    return true
  end
  pbMessage("There are no Gows within a 5x5 radius of this Collector with Gow Stones ready.")
  return false
end

def find_gows(x, y)
  gows = []
  tile = []
  [-2,-1, 0, 1, 2].each do |i|
    [-2,-1, 0, 1, 2].each do |j|
      tile = [i + x, j + y]
      next if tile[0] < 0 || tile[0] >= $game_map.width || tile[1] < 0 || tile[1] >= $game_map.height
      event = $game_map.event_at(tile[0], tile[1])
      next if !event
      gows.push(event)
    end
  end
  return gows
end