EventHandlers.add(:on_player_interact, :work_bench,
  proc {
    facing_terrain = $game_player.pbFacingTerrainTag
    next unless facing_terrain
    sel_item = $bag.pockets[1][$game_temp.inventory_selected]
    case facing_terrain.id
    when :DeepWater # Workbench
      if !sel_item || !sel_item.is_a?(Array)
        pbMessage("It's a workbench, used to upgrade tools, make fences and food.")
        next
      end
      case sel_item[0]
      when :MAINTOOL, :MAINTOOL2
        cost = sel_item[0]==:MAINTOOL ? 100 : 500
        if pbConfirmMessage(_INTL("Would you like to upgrade your {1}? (Cost: {2} Wood)", GameData::Item.get(sel_item[0]).name, cost))
          if $bag.quantity(:WOOD) >= cost
            $bag.remove(:WOOD,cost)
            pbUpgradeTool
          else
            pbPlayBuzzerSE
            pbMessage(_INTL("You don't have enough Wood, you need at least {1}!", cost))
          end
        end
      when :WOOD
        pbMakeItem(:FENCE, :WOOD)
      when :STONE
        pbMakeItem(:COLLECTOR, :STONE, 10)
      when :CROP
        pbMakeItem(:PURENRG, :CROP, 100)
      when :PLANT
        pbMakeItem(:POTTEDPLANT, :PLANT)
      when :FLOWER
        pbMakeItem(:POTTEDFLOWER, :FLOWER)
      when :LEATHER
        max_stored_bag_upgrades = 11-$player.inventory_size
        stored_bag_upgrades = $bag.quantity(:BAGUPGRADE) + $player.quantity_in_chest?(:BAGUPGRADE)
        pbMakeItem(:BAGUPGRADE, :LEATHER, 25, stored_bag_upgrades < max_stored_bag_upgrades)
      when :DROP
        pbMakeItem(:GOWREADER, :DROP, 50, !$bag.has(:GOWREADER) && !$player.has_in_chest?(:GOWREADER))
      else
        # Nothing
      end
    when :WaterfallCrest # Collector
      facing = pbFacingTile($game_player.direction, $game_player)
      pbCollector(facing[1], facing[2])
    when :TallGrass # Bin
      if !sel_item || !sel_item.is_a?(Array)
        pbMessage("It's a trash can, interacting with this using any consumable item will destory it.")
        next
      end
      item = GameData::Item.get(sel_item[0])
      if item.is_mega_stone? || item.is_snag_ball?
        pbMessage("Cannot throw away your #{item.name}!")
        next
      end
      pbSEPlay("SFX_FAINT_FALL_QUIET")
      $bag.remove(sel_item[0])
      $game_temp.inventory_menu.pbUpdate
    when :UnderwaterGrass # Chest
      chest_scene = Chest.new
      chest_scene.pbStartScene
    end
  }
)

def pbMakeItem(item, cost_item, cost_amount=1, additional_check = true)
  return false unless additional_check
  if $bag.quantity(cost_item) < cost_amount
    pbMessage("You will need #{cost_amount-$bag.quantity(cost_item)} more #{GameData::Item.get(cost_item).name} to craft a #{GameData::Item.get(item).name}.")
    return false
  end
  if pbCheckInventorySpace || $bag.quantity(cost_item) == cost_amount || $bag.quantity(item) > 0
    pbSEPlay("SFX_SLOTS_REWARD")
    $bag.remove(cost_item,cost_amount)
    $bag.add(item,1)
    $game_temp.inventory_menu.pbUpdate
  end
  return true
end

def pbUseTool
  sel_item = $bag.pockets[1][$game_temp.inventory_selected]
  return false if !sel_item || !sel_item.is_a?(Array)
  facing = $game_player.moving? ? [$game_player.direction, $game_player.x, $game_player.y] : pbFacingTile($game_player.direction, $game_player)
  return false if facing.nil?
  facing_event = $game_player.pbFacingEvent
  case sel_item[0]
  when :MAINTOOL, :MAINTOOL2, :MAINTOOL3
    if $game_player.pbFacingTerrainTag.id == :Water && $game_map.passable?(facing[1], facing[2], $game_player.direction, $game_player) &&
      !facing_event
      thistile = $map_factory.getRealTilePos($game_player.map.map_id, facing[1], facing[2])
      map = $map_factory.getMap(thistile[0])
      tile_id = map.data[thistile[1], thistile[2], 1]
      item_get = (map.get_tile(thistile[1], thistile[2], 1) == 394) ? :WOOD : :STONE
      return false if tile_id.nil?
      return false if map.get_tile(thistile[1], thistile[2], 0) == 387
      return false if GameData::TerrainTag.try_get(map.terrain_tags[tile_id]).id != :Water
      return false if !pbCheckInventorySpace && $bag.quantity(item_get) < 1
      pbSEPlay("SFX_SNARE_9")
      map.erase_tile(thistile[1], thistile[2], 1)
      $bag.add(item_get,1)
      $game_temp.inventory_menu.pbUpdate
    elsif $game_player.pbFacingTerrainTag.id == :Grass && $game_map.passable?(facing[1], facing[2], $game_player.direction, $game_player) &&
      !facing_event
      thistile = $map_factory.getRealTilePos($game_player.map.map_id, facing[1], facing[2])
      map = $map_factory.getMap(thistile[0])
      [2, 1, 0].each do |i|
        tile_id = map.data[thistile[1], thistile[2], i]
        next if tile_id.nil?
        return if map.get_tile(thistile[1], thistile[2], i)!=0 && i != 0
        next if GameData::TerrainTag.try_get(map.terrain_tags[tile_id]).id != :Grass
        map.set_tile(thistile[1], thistile[2], i, 386)
        spriteset&.addUserAnimation(Settings::GRASS_ANIMATION_ID, thistile[1], thistile[2], true, 1)
        (sel_item[0] == :MAINTOOL) ? seed_chance = 5 : (sel_item[0] == :MAINTOOL2) ? seed_chance = 3 : seed_chance = 1
        pbGetSeed if rand(seed_chance) == 1
        if rand(10) == 1 && (pbCheckInventorySpace || $bag.quantity(:FLOWER) > 0)
          $bag.add(:FLOWER,1)
          $game_temp.inventory_menu.pbUpdate
        end
      end
    elsif $game_player.pbFacingTerrainTag.id == :Waterfall && !facing_event
      if sel_item[0] == :MAINTOOL
        pbPlayBuzzerSE
        $game_switches[75] = true
        pbMessage("You need a stronger tool to break stones!")
        $game_switches[75] = false
        return false
      end
      thistile = $map_factory.getRealTilePos($game_player.map.map_id, facing[1], facing[2])
      map = $map_factory.getMap(thistile[0])
      tile_id = map.data[thistile[1], thistile[2], 1]
      return false if tile_id.nil?
      return false if GameData::TerrainTag.try_get(map.terrain_tags[tile_id]).id != :Waterfall
      return false if !pbCheckInventorySpace && $bag.quantity(:STONE) < 1
      if pbMineAnimation([thistile[1], thistile[2]], sel_item[0])
        pbSEPlay("SFX_CUT")
        map.erase_tile(thistile[1], thistile[2], 1)
        $bag.add(:STONE,rand(1..3))
        $game_temp.inventory_menu.pbUpdate
        return true
      end
      return false
    elsif $game_player.pbFacingTerrainTag.id == :DeepWater
      return pbDestroyAndGet(facing[1], facing[2], :DeepWater, :WORKBENCH, 2, true)
    elsif $game_player.pbFacingTerrainTag.id == :Ice
      return pbDestroyAndGet(facing[1], facing[2], :Ice, :CROP, 1)
    elsif $game_player.pbFacingTerrainTag.id == :TallGrass
      return pbDestroyAndGet(facing[1], facing[2], :TallGrass, :BIN, 2, true)
    elsif $game_player.pbFacingTerrainTag.id == :UnderwaterGrass
      return pbDestroyAndGet(facing[1], facing[2], :UnderwaterGrass, :CHEST, 2, true)
    elsif $game_player.pbFacingTerrainTag.id == :WaterfallCrest
      return pbDestroyAndGet(facing[1], facing[2], :WaterfallCrest, :COLLECTOR, 2)
    elsif $game_player.pbFacingTerrainTag.id == :Bridge
      return pbDestroyAndGet(facing[1], facing[2], :Bridge, :POTTEDPLANT, 2, false, false)
    elsif $game_player.pbFacingTerrainTag.id == :Puddle
      return pbDestroyAndGet(facing[1], facing[2], :Puddle, :FLOWER, 2, false, false)
    elsif $game_player.pbFacingTerrainTag.id == :NoEffect
      return pbDestroyAndGet(facing[1], facing[2], :NoEffect, :POTTEDFLOWER, 2, false, false)
    elsif $game_player.pbFacingTerrainTag.id == :Plant
      return pbDestroyAndGet(facing[1], facing[2], :Plant, :PLANT, 2, false, false)
    elsif $game_player.pbFacingTerrainTag.ledge
      thistile = $map_factory.getRealTilePos($game_player.map.map_id, facing[1], facing[2])
      map = $map_factory.getMap(thistile[0])
      tile_id = map.data[thistile[1], thistile[2], 2]
      return false if map.get_tile(thistile[1], thistile[2], 0) == 387
      return false if !GameData::TerrainTag.try_get(map.terrain_tags[tile_id]).ledge
      return false if !pbCheckInventorySpace && $bag.quantity(:FENCE) < 1
      map.erase_tile(thistile[1], thistile[2], 2)
      $bag.add(:FENCE,1)
      $game_temp.inventory_menu.pbUpdate
      pbUpdateNeighbourFences(map, thistile[1], thistile[2])
      pbSEPlay("SFX_BALL_TOSS")
      pbWait(16)
      return true
    elsif $game_player.pbFacingTerrainTag.id == :Rock && !$game_player.moving?
      thistile = $map_factory.getRealTilePos($game_player.map.map_id, facing[1], facing[2])
      map = $map_factory.getMap(thistile[0])
      tile_id = map.data[thistile[1], thistile[2], 0]
      return false if !GameData::TerrainTag.try_get(map.terrain_tags[tile_id]).id == :Rock
      return false if !pbCheckInventorySpace && $bag.quantity(:CROP) < 1
      map.set_tile(thistile[1], thistile[2], 0, 386)
      spriteset&.addUserAnimation(Settings::GRASS_ANIMATION_ID, thistile[1], thistile[2], true, 1)
      get_amt = rand(1..2)
      get_amt += 1 if sel_item[0] == :MAINTOOL2
      get_amt += rand(2..4) if sel_item[0] == :MAINTOOL3
      $bag.add(:CROP,get_amt)
      $game_temp.inventory_menu.pbUpdate
      pbSEPlay("SFX_BALL_TOSS")
      pbWait(16)
    elsif facing_event
      if facing_event.name[/CutTree/]
        case facing_event.direction
        when 2 # Grown
          return false if !pbCheckInventorySpace && $bag.quantity(:WOOD) < 1
          if pbMineAnimation([facing_event.x, facing_event.y], sel_item[0])
            facing_event.direction = 4
            pbSEPlay("SFX_BALL_TOSS")
            pbWait(16)
            dropAmt = rand(1..4)
            dropAmt += 2 if sel_item[0] == :MAINTOOL2
            dropAmt *= 3 if sel_item[0] == :MAINTOOL3
            pbReceiveItem(:WOOD,dropAmt)
            if rand(10) == 1 && (pbCheckInventorySpace || $bag.quantity(:PLANT) > 0)
              $bag.add(:PLANT,1)
              $game_temp.inventory_menu.pbUpdate
            end
            return true
          end
          return false
        when 4 # Stump
          if sel_item[0] == :MAINTOOL3
            return false if !pbCheckInventorySpace && $bag.quantity(:WOOD) < 1
            if pbMineAnimation([facing_event.x, facing_event.y], sel_item[0])
              facing_event.direction = 6
              pbSEPlay("SFX_BALL_TOSS")
              pbWait(16)
              pbReceiveItem(:WOOD,rand(4..9))
              return true
            end
            return false
          else
            pbMessage("Can't remove a tree stump!")
          end
        when 6 # Baby
          # Nothing
        else # Nothing
        end
      end
      if facing_event.name[/Animal/]
        anim = facing_event.variable
        return false if anim.nil?
        return false if !anim.is_a?(Animal)
        return anim.slay
      end
    end
  when :FENCE
    return pbPlaceTile(facing_event, facing[1], facing[2], 2, 411, :FENCE, "SFX_INTRO_CRASH", 0, true)
  when :CROP
    return pbPlaceTile(facing_event, facing[1], facing[2], 1, 390, :CROP, "SFX_SNARE_9")
  when :WORKBENCH
    return pbPlaceTile(facing_event, facing[1], facing[2], 2, 393, :WORKBENCH, "SFX_INTRO_CRASH", 0, false, true)
  when :BIN
    return pbPlaceTile(facing_event, facing[1], facing[2], 2, 419, :BIN, "SFX_INTRO_CRASH", 0, false, true)
  when :CHEST
    return pbPlaceTile(facing_event, facing[1], facing[2], 2, 420, :CHEST, "SFX_INTRO_CRASH", 0, false, true)
  when :COLLECTOR
    return pbPlaceTile(facing_event, facing[1], facing[2], 2, 412, :COLLECTOR, "SFX_INTRO_CRASH")
  when :SEED
    return pbPlaceTile(facing_event, facing[1], facing[2], 0, 388, :SEED, "SFX_SNARE_9", 386)
  when :POTTEDPLANT
    return pbPlaceTile(facing_event, facing[1], facing[2], 2, 404, :POTTEDPLANT, "SFX_INTRO_CRASH")
  when :PLANT
    return pbPlaceTile(facing_event, facing[1], facing[2], 2, 413, :PLANT, "SFX_SNARE_9", 385, false, false, 0)
  when :POTTEDFLOWER
    return pbPlaceTile(facing_event, facing[1], facing[2], 2, 406, :POTTEDFLOWER, "SFX_INTRO_CRASH")
  when :FLOWER
    return pbPlaceTile(facing_event, facing[1], facing[2], 2, 405, :FLOWER, "SFX_SNARE_9", 385, false, false, 0)
  when :WOOD
    return pbPlaceTile(facing_event, facing[1], facing[2], 1, 394, :WOOD, "SFX_MUTED_SNARE_4")
  when :STONE
    return pbPlaceTile(facing_event, facing[1], facing[2], 1, 397, :STONE, "SFX_SNARE_9")
  when :BAGUPGRADE
    pbSEPlay("SFX_BALL_POOF")
    $bag.remove(:BAGUPGRADE)
    $game_temp.inventory_menu.pbUpdate
    pbGainInventorySlot
  when :PURENRG
    pbSEPlay("SFX_BALL_POOF")
    $bag.remove(:PURENRG)
    $game_temp.inventory_menu.pbUpdate
    $player.gain_max_stamina
  when :REPEL
    newevent = RPG::Event.new(facing[1], facing[2])
    newevent.pages[0].trigger = 0
    newevent.id = $game_map.events.length+1
    newevent.name = "New Event"
    Compiler::push_text(newevent.pages[0].list,"hello")
    event = Game_Event.new($game_map.map_id, newevent, $game_map)
    event.character_name = "base"
    event.character_hue = 0
    event.opacity = 255
    event.through = false
    $game_map.events[$game_map.events.length+1] = event
    $game_map.events[$game_map.events.length].need_refresh = true
    $game_map.events[$game_map.events.length].direction = 2
    $game_map.events[$game_map.events.length].start
    $game_map.refresh
  end
  return true
end

def pbDestroyAndGet(x, y, terrain_tag, item, layer, ignore_road = false, use_stamina = true)
  thistile = $map_factory.getRealTilePos($game_player.map.map_id, x, y)
  map = $map_factory.getMap(thistile[0])
  tile_id = map.data[thistile[1], thistile[2], layer]
  return false if map.get_tile(thistile[1], thistile[2], 0) == 387 && !ignore_road
  return false if GameData::TerrainTag.try_get(map.terrain_tags[tile_id]).id != terrain_tag
  return false if !pbCheckInventorySpace && $bag.quantity(item) < 1
  if pbMineAnimation([thistile[1], thistile[2]], :MAINTOOL3, use_stamina)
    map.erase_tile(thistile[1], thistile[2], layer)
    $bag.add(item,1)
    $game_temp.inventory_menu.pbUpdate
    pbSEPlay("SFX_BALL_TOSS")
    pbWait(16)
    return true
  end
  return false
end

def pbPlaceTile(facing_event=nil, x=0, y=0, layer=0, tileid=0, item=nil, sound="", check_tile = 0, check_fences = false, ignore_road = false, check_tile_layer = layer)
  if $game_map.passable?(x, y, $game_player.direction, $game_player) &&
    !facing_event && !$game_player.moving?
    thistile = $map_factory.getRealTilePos($game_player.map.map_id, x, y)
    map = $map_factory.getMap(thistile[0])
    tile_id = map.data[thistile[1], thistile[2], layer]
    return false if (ignore_road) ? thistile[2] > 18 : map.get_tile(thistile[1], thistile[2], 0) == 387
    return false if map.get_tile(thistile[1], thistile[2], check_tile_layer) != check_tile
    for i in 0..2
      next if i <= layer
      return false if map.get_tile(thistile[1], thistile[2], i) != 0
    end
    $bag.remove(item,1)
    $game_temp.inventory_menu.pbUpdate
    pbSEPlay(sound)
    map.set_tile(thistile[1], thistile[2], layer, tileid)
    pbUpdateNeighbourFences(map, thistile[1], thistile[2]) if check_fences
    return true
  end
  return false
end

def pbGetSeed
  return false if !pbCheckInventorySpace && $bag.quantity(:SEED) < 1
  pbSEPlay("SFX_SLOTS_REWARD")
  amt = 1
  amt += (rand(9..15)/10).round if rand(5) == 1
  $bag.add(:SEED,amt)
  $game_temp.inventory_menu.pbUpdate
end

def pbUpgradeTool
  return false if !$bag.has?(:MAINTOOL) && !$bag.has?(:MAINTOOL2)
  if $bag.has?(:MAINTOOL)
    $bag.replace_item(:MAINTOOL, :MAINTOOL2)
    $game_temp.inventory_menu.pbUpdate
    pbMessage(_INTL("\\me[SFX_GET_ITEM_2]Your {1} has been upgraded to a {2}!\\wtnp[30]",GameData::Item.get(:MAINTOOL).name,GameData::Item.get(:MAINTOOL2).name))
  else
    $bag.replace_item(:MAINTOOL2, :MAINTOOL3)
    $game_temp.inventory_menu.pbUpdate
    pbMessage(_INTL("\\me[SFX_GET_ITEM_2]Your {1} has been upgraded to a {2}!\\wtnp[30]",GameData::Item.get(:MAINTOOL2).name,GameData::Item.get(:MAINTOOL3).name))
  end
end

def pbMineAnimation(xy, tool, use_stamina = true)
  spriteset&.addUserAnimation(Settings::DUST_ANIMATION_ID, xy[0], xy[1], true, 1)
  pbWait(14) if tool != :MAINTOOL3
  if tool == :MAINTOOL
    spriteset&.addUserAnimation(Settings::DUST_ANIMATION_ID, xy[0], xy[1], true, 1)
    pbWait(14)
  end
  return use_stamina ? $player.modify_stamina(-1) : true
end

def pbOvernightUpdate
  stone_count = pbGetMapTileData(395).length + pbGetMapTileData(396).length
  announcements = []
  for x in 0..$game_map.width
    for y in 0..$game_map.height
      thistile = $map_factory.getRealTilePos($game_player.map.map_id, x, y)
      next if !thistile
      map = $map_factory.getMap(thistile[0])
      map.set_tile(thistile[1], thistile[2], 0, 389) if map.get_tile(thistile[1], thistile[2], 0) == 388 && (rand(2) == 1 || GameData::Weather.get($game_screen.weather_type).category == :Rain) # 50% for crops to grow unless rains
      if map.get_tile(thistile[1], thistile[2], 0) == 386 && next_to_grass(map, x, y) && rand(10) == 1
        map.set_tile(thistile[1], thistile[2], 0, 385)
      end
      if stone_count < 50
        if map.get_tile(thistile[1], thistile[2], 0) == 385 && map.get_tile(thistile[1], thistile[2], 1) == 0 &&
          map.get_tile(thistile[1], thistile[2], 2) == 0 && !$game_map.event_at(x, y) && rand(0..50) == 1 # 2% chance for every free tile to get a stone
          map.set_tile(thistile[1], thistile[2], 1, [395, 396].sample)
          stone_count += 1
        end
      end
    end
  end
  $game_screen.weather(:None, 9, 0)
  for event in $game_map.events
    e = event[1]
    if e.name[/CutTree/]
      e.direction -= 2 if e.direction != 2
    end
    if e.name[/Animal/]
      anim = e.variable
      next if anim.nil?
      next if !anim.is_a?(Animal)
      if anim.despawn && rand(2) != 2 # 66.67% chance of an untamed Gow to despawn
        anim.death
        next
      end
      name = anim.name
      ret = anim.age_up
      next unless ret
      case ret
      when :age then announcements.push("#{name} passed away overnight to natural causes...")
      when :hunger then announcements.push("#{name} passed away overnight due to malnutrition / hunger...")
      when :unhappy then announcements.push("#{name} passed away overnight due to poor health conditions...")
      else announcements.push("#{name} passed away overnight to unknown causes...")
      end
    end
  end
  $player.stamina = $player.max_stamina
  $player.next_day
  $game_temp.inventory_menu.pbUpdate
  Inventory.sell_stored_items
  pbCreateAnimals
  announcements.each do |announ|
    pbMessage(announ)
  end
  new_weather = $game_map.metadata.weather
  $game_screen.weather(new_weather[0], 9, 0) if rand(100) < new_weather[1]
end

def next_to_grass(map, x, y)
  ret = false
  for i in -1..1
    for j in -1..1
      next if i == 0 && j == 0 # Self
      next unless i == 0 || j == 0 # Not connected
      if map.get_tile(x + i, y + j, 0) == 385
        ret = true
        break
      end
    end
  end
  return ret
end

def pbUpdateNeighbourFences(map, x, y)
  for i in -1..1
    for j in -1..1
      next if x+i < 0 || x+i >= $game_map.width || y+j < 0 || y+j >= $game_map.height
      pbCheckNeighbourFences(map, x+i, y+j) if GameData::TerrainTag.try_get(map.terrain_tags[map.data[x+i, y+j, 2]]).ledge
    end
  end
end

def pbUpdateEdgeFences
  map = $map_factory.getMap($game_map.map_id)
  for x in 0...$game_map.width
    for y in 0...$game_map.height
      next if x > 0 && x < $game_map.width-1 && y > 0 && y < $game_map.height-1
      next unless GameData::TerrainTag.try_get(map.terrain_tags[map.data[x, y, 2]]).ledge
      pbCheckNeighbourFences(map, x, y)
    end
  end
end

def pbCheckNeighbourFences(map, x, y)
  n = (y-1 > -1) ? GameData::TerrainTag.try_get(map.terrain_tags[map.data[x, y-1, 2]]).ledge : $PokemonSystem.fencesconnect==1
  s = (y+1 < $game_map.height) ? GameData::TerrainTag.try_get(map.terrain_tags[map.data[x, y+1, 2]]).ledge : $PokemonSystem.fencesconnect==1
  w = (x-1 > -1) ? GameData::TerrainTag.try_get(map.terrain_tags[map.data[x-1, y, 2]]).ledge : $PokemonSystem.fencesconnect==1
  e = (x+1 < $game_map.width) ? GameData::TerrainTag.try_get(map.terrain_tags[map.data[x+1, y, 2]]).ledge : $PokemonSystem.fencesconnect==1
  map.set_tile(x, y, 2, 409); return true if n && s && w && e
  map.set_tile(x, y, 2, 410); return true if n && s && !e
  map.set_tile(x, y, 2, 408); return true if n && s && e
  map.set_tile(x, y, 2, 417); return true if n && w && e
  map.set_tile(x, y, 2, 401); return true if s && w && e
  map.set_tile(x, y, 2, 416); return true if n && e
  map.set_tile(x, y, 2, 418); return true if n && w
  map.set_tile(x, y, 2, 400); return true if s && e
  map.set_tile(x, y, 2, 402); return true if s && !e
  map.set_tile(x, y, 2, 403); return true if e && w
  map.set_tile(x, y, 2, 418); return true if n
  map.set_tile(x, y, 2, 402); return true if s
  map.set_tile(x, y, 2, 403); return true if e
  map.set_tile(x, y, 2, 411); return true
  return false
end